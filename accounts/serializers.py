from django.contrib.auth import authenticate
from django.http import HttpResponse
from rest_framework import serializers, request

from accounts.models import User


# # User Serializer
# class UserSerializer(serializers.ModelSerializer):
#     class Meta:
#         model = User
#         fields = ('id', 'email', 'password')


# Register Serializer
class RegisterSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = ('id', 'first_name', 'last_name', 'email', 'password')
        extra_kwargs = {'password': {'write_only': True}}

    def save(self):
        user = User(
            first_name=self.validated_data['first_name'],
            last_name=self.validated_data['last_name'],
            email=self.validated_data['email'],
        )
        password = self.validated_data['password']
        user.set_password(password)
        # user = User.objects.create_user(validated_data['email'], validated_data['password'])
        user.save()

        return user


class LoginSerializer(serializers.Serializer):
    # first_name = serializers.CharField()
    # last_name = serializers.CharField()
    email = serializers.EmailField()
    password = serializers.CharField()

    def validate(self, data):
        user = authenticate(**data)
        print(user)
        if user and user.is_active:
            return user
        raise serializers.ValidationError("incorrect")
#logout
