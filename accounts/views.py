from rest_framework import generics, mixins
from rest_framework.response import Response
from rest_framework.authtoken.models import Token
from accounts.serializers import RegisterSerializer, LoginSerializer


class RegisterAPI(generics.GenericAPIView):
    serializer_class = RegisterSerializer

    def post(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        data = {}
        user = serializer.save()
        data['response'] = "Succesfully registered a new user."
        data['first_name'] = user.first_name
        data['last_name'] = user.last_name
        data['email'] = user.email
        token = Token.objects.get(user=user).key
        data['token'] = token
        return Response(data)


class LoginAPI(generics.GenericAPIView):
    serializer_class = LoginSerializer

    def post(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        data = {}
        user = serializer.validated_data
        # data['first_name'] = user.first_name
        # data['last_name'] = user.last_name
        data['email'] = user.email
        token = Token.objects.get(user=user).key
        data['token'] = token
        return Response(data)
