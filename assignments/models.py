from datetime import datetime
from django.db import models


class Assignment(models.Model):
    file = models.FileField(blank=True)
    obtained_marks = models.DecimalField(max_digits=5, decimal_places=2, default=0)
    title = models.CharField(max_length=50)
    total_marks = models.DecimalField(max_digits=5, decimal_places=2, default=0)
    created_at = models.DateTimeField(default=datetime.now(), blank=True, editable=False)
    deadline = models.DateTimeField(default=datetime.now(), blank=True)
    submitted_at = models.DateTimeField(default=datetime.now(), blank=True)
    updated_at = models.DateTimeField(default=datetime.now(), blank=True)
