from django.urls import path
from django.views.decorators.csrf import csrf_exempt

from assignments.views import AssignmentDetailView, AssignmentListCreateView

urlpatterns = [
    path('', csrf_exempt(AssignmentListCreateView.as_view()), name='AssignmentListCreateView'),
    path('<pk>', AssignmentDetailView.as_view()),
]