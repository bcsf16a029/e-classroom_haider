from django.contrib import admin
from classrooms.models import Classroom, Announcement
# Register your models here.
admin.site.register(Classroom)
admin.site.register(Announcement)
