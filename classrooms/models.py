from datetime import datetime

from django.db import models

from courses.models import Course


class TimeStampMixin(models.Model):
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    class Meta:
        abstract = True


# Create your models here.
class Classroom(TimeStampMixin):
    course = models.ForeignKey(Course, on_delete=models.CASCADE, default=None)

    class_name = models.CharField(max_length=200, unique=True)
    no_of_students = models.IntegerField(null=True, blank=True)


class Announcement(TimeStampMixin):
    classroom = models.ForeignKey(Classroom, on_delete=models.CASCADE, default=None)

    title = models.CharField(max_length=500)
    content = models.TextField(default=None, null=True, blank=True)
