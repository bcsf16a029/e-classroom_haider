from django.urls import path
from django.views.decorators.csrf import csrf_exempt

from classrooms.views import ClassroomListCreateView, ClassroomDetailView, AnnouncementDetailView, \
    AnnouncementListCreateView

urlpatterns = [
    path("", csrf_exempt(ClassroomListCreateView.as_view()), name='ClassroomListCreateView'),
    path("<pk>", ClassroomDetailView.as_view()),
    path("announcements/", AnnouncementListCreateView.as_view()),
    path("announcements/<pk>", AnnouncementDetailView.as_view())
]
