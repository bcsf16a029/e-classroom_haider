from django.contrib import admin

from courses.models import Course, Course_Content
# Register your models here.
admin.site.register(Course)
admin.site.register(Course_Content)
