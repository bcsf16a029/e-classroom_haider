from datetime import datetime

from django.db import models


# Create your models here.
from django.utils import timezone


class TimeStampMixin(models.Model):
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    class Meta:
        abstract = True


class Course(TimeStampMixin):
    course_name = models.CharField(max_length=200)
    course_fee = models.IntegerField(null=True, blank=True)
    no_of_classes = models.IntegerField(null=True, blank=True)


class Course_Content(TimeStampMixin):
    course = models.ForeignKey(Course, on_delete=models.CASCADE)

    content = models.CharField(max_length=200)