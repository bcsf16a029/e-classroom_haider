from rest_framework import serializers


from courses.models import Course, Course_Content


class CourseSerializer(serializers.ModelSerializer):
    class Meta:
        model = Course
        fields = '__all__'


class CourseContentSerializer(serializers.ModelSerializer):
    class Meta:
        model = Course_Content
        fields = '__all__'
