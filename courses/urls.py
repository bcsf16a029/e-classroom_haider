from django.urls import path
from django.views.decorators.csrf import csrf_exempt

from courses.views import CourseListCreateView, CourseDetailView, CourseContentListCreateView, CourseContentDetailView

urlpatterns = [
    path("", csrf_exempt(CourseListCreateView.as_view()), name='CourseListCreateView'),
    path("<pk>", CourseDetailView.as_view()),
    path("content/", CourseContentListCreateView.as_view()),
    path("content/<pk>", CourseContentDetailView.as_view())
]
