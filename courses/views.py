from rest_framework import generics

from courses.serializers import CourseSerializer, CourseContentSerializer

from courses.models import Course, Course_Content

from django.utils.decorators import method_decorator

from django.views.decorators.csrf import csrf_exempt


@method_decorator(csrf_exempt, name='dispatch')
class CourseListCreateView(generics.ListCreateAPIView):
    queryset = Course.objects.all()
    serializer_class = CourseSerializer

    def dispatch(self, *args, **kwargs):
        return super().dispatch(*args, **kwargs)


class CourseDetailView(generics.RetrieveUpdateDestroyAPIView):
    queryset = Course.objects.all()
    serializer_class = CourseSerializer


class CourseContentListCreateView(generics.ListCreateAPIView):
    queryset = Course_Content.objects.all()
    serializer_class = CourseContentSerializer


class CourseContentDetailView(generics.RetrieveUpdateDestroyAPIView):
    queryset = Course_Content.objects.all()
    serializer_class = CourseContentSerializer
