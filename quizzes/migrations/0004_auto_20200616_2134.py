# Generated by Django 3.0.7 on 2020-06-16 16:34

import datetime
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('quizzes', '0003_auto_20200612_1839'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='question',
            name='assignment',
        ),
        migrations.AddField(
            model_name='question',
            name='choice_four',
            field=models.TextField(blank=True, null=True),
        ),
        migrations.AddField(
            model_name='question',
            name='choice_one',
            field=models.TextField(blank=True, null=True),
        ),
        migrations.AddField(
            model_name='question',
            name='choice_three',
            field=models.TextField(blank=True, null=True),
        ),
        migrations.AddField(
            model_name='question',
            name='choice_two',
            field=models.TextField(blank=True, null=True),
        ),
        migrations.AddField(
            model_name='question',
            name='correct_option',
            field=models.TextField(blank=True, null=True),
        ),
        migrations.AlterField(
            model_name='quiz',
            name='deadline',
            field=models.DateTimeField(default=datetime.datetime(2020, 6, 16, 21, 34, 15, 655158)),
        ),
    ]
