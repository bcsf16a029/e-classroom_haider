# Generated by Django 3.0.7 on 2020-06-16 16:39

import datetime
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('quizzes', '0004_auto_20200616_2134'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='question',
            options={'ordering': ('position',)},
        ),
        migrations.AddField(
            model_name='question',
            name='choice',
            field=models.CharField(blank=True, max_length=500, null=True, verbose_name='Question'),
        ),
        migrations.AddField(
            model_name='question',
            name='position',
            field=models.IntegerField(blank=True, null=True, verbose_name='position'),
        ),
        migrations.AlterField(
            model_name='choice',
            name='question',
            field=models.ForeignKey(default=None, null=True, on_delete=django.db.models.deletion.CASCADE, related_name='choices', to='quizzes.Question'),
        ),
        migrations.AlterField(
            model_name='quiz',
            name='deadline',
            field=models.DateTimeField(default=datetime.datetime(2020, 6, 16, 21, 39, 41, 517993)),
        ),
        migrations.AlterUniqueTogether(
            name='question',
            unique_together={('statement', 'position'), ('statement', 'choice')},
        ),
        migrations.RemoveField(
            model_name='question',
            name='choice_four',
        ),
        migrations.RemoveField(
            model_name='question',
            name='choice_one',
        ),
        migrations.RemoveField(
            model_name='question',
            name='choice_three',
        ),
        migrations.RemoveField(
            model_name='question',
            name='choice_two',
        ),
        migrations.RemoveField(
            model_name='question',
            name='correct_option',
        ),
    ]
