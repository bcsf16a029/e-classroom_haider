from datetime import datetime
from assignments.models import Assignment
from django.db import models


class Quiz(models.Model):
    deadline = models.DateTimeField(default=datetime.now())

    created_at = models.DateTimeField(auto_now_add=True, blank=True, null=True)
    end_date = models.DateTimeField(null=True, blank=True)
    start_date = models.DateTimeField(null=True, blank=True)
    updated_at = models.DateTimeField(auto_now=True, blank=True, null=True)


class Question(models.Model):
    #assignment = models.ForeignKey(Assignment, on_delete=models.CASCADE, default=None, null=True)
    quiz = models.ForeignKey(Quiz, on_delete=models.CASCADE, default=None, null=True)
    statement = models.TextField(null=True, blank=True)
    choice_one = models.TextField(null=True, blank=True)
    choice_two = models.TextField(null=True, blank=True)
    choice_three = models.TextField(null=True, blank=True)
    choice_four = models.TextField(null=True, blank=True)
    correct_option = models.TextField(null=True, blank=True)

    quiz_status = models.BooleanField(default=True)
    total_marks = models.DecimalField(max_digits=5, decimal_places=2, default=0)
    obtained_marks = models.DecimalField(max_digits=5, decimal_places=2, default=0)

    created_at = models.DateTimeField(auto_now_add=True, blank=True, null=True)
    expired_at = models.DateTimeField(auto_now=True, blank=True, null=True)
    updated_at = models.DateTimeField(auto_now=True, blank=True, null=True)


class Choice(models.Model):
    question = models.ForeignKey(Question, on_delete=models.CASCADE, default=None, null=True)
    # correct_option = models.IntegerField(primary_key=True)
    # choice_one = models.TextField(null=True, blank=True)
    # choice_two = models.TextField(null=True, blank=True)
    # choice_three = models.TextField(null=True, blank=True)
    # choice_four = models.TextField(null=True, blank=True)
    choice_text = models.TextField(null=True,blank=True)
    vote = models.IntegerField(default=0)

    created_at = models.DateTimeField(auto_now_add=True, blank=True, null=True)
    # end_date = models.DateTimeField(null=True, blank=True)
    # start_date = models.DateTimeField(null=True, blank=True)
    updated_at = models.DateTimeField(auto_now=True, blank=True, null=True)
