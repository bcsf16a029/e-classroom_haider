from rest_framework import serializers
from quizzes.models import Quiz, Choice, Question


class QuizSerializers(serializers.ModelSerializer):
    class Meta:
        model = Quiz
        fields = '__all__'


class ChoiceSerializers(serializers.ModelSerializer):
    class Meta:
        model = Choice
        fields = '__all__'


class QuestionSerializers(serializers.ModelSerializer):
    class Meta:
        model = Question
        fields = '__all__'
