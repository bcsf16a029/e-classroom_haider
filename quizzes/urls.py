from django.urls import path
from quizzes.views import QuestionDetailView, QuestionListCreateView, \
    ChoiceDetailView, ChoiceListCreateView, \
    QuizDetailView, QuizListCreateView
from django.views.decorators.csrf import csrf_exempt

urlpatterns = [
    path('', csrf_exempt(QuizListCreateView.as_view()), name='QuizListCreateView'),
    path('<pk>', QuizDetailView.as_view()),
    path('choice/', ChoiceListCreateView.as_view()),
    path('choice/<pk>', ChoiceDetailView.as_view()),
    path('question/', QuestionListCreateView.as_view()),
    path('question/<pk>', QuestionDetailView.as_view()),
]
